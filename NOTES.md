# Notes
Notes for zenport go assignment

## Structure change
- Add DB host and port config to adapter

## Docker compose
Usage: `docker-compose up`

## Database
MySql

### User
- username: zenport
- password: zenport

### DB info
- database: arena
- table knight
    - id
    - name
    - strength
    - weapon_power

### docker image
1. Build image
```
docker build docker/mysql/ -t go_assignment_database
```
2. Start docker container, listen on port 3306
```
docker run -p 3306:3306 -e MYSQL_DATABASE=arena -e MYSQL_USER=zenport -e MYSQL_PASSWORD=zenport -e MYSQL_ALLOW_EMPTY_PASSWORD=true assignment_db
```
