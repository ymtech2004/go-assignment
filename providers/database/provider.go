package database

import (
	"database/sql"
	"fmt"

	"gitlab.com/zenport.io/go-assignment/engine"

	_ "github.com/go-sql-driver/mysql"
)

type Provider struct {
	db *sql.DB
}

func (provider *Provider) GetKnightRepository() engine.KnightRepository {
	return &knightRepository{provider.db}
}

func (provider *Provider) Close() {
	provider.db.Close()
}

func NewProvider(host, port string) *Provider {
	db, err := sql.Open("mysql", fmt.Sprintf("zenport:zenport@(%s:%s)/arena", host, port))
	if err != nil {
		panic(err)
	}
	return &Provider{db}
}
