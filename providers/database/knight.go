package database

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/zenport.io/go-assignment/domain"
)

type knightRepository struct {
	db *sql.DB
}

func (repository *knightRepository) Find(ID string) *domain.Knight {
	var k domain.Knight
	err := repository.db.QueryRow("SELECT id, name, strength, weapon_power FROM knight where id = ?", ID).Scan(&k.ID, &k.Name, &k.Strength, &k.WeaponPower)
	if err != nil {
		return nil
	}

	return &k
}

func (repository *knightRepository) FindAll() []*domain.Knight {
	list := make([]*domain.Knight, 0)
	// Execute the query
	results, err := repository.db.Query("SELECT id, name, strength, weapon_power FROM knight")
	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}

	for results.Next() {
		var k domain.Knight
		err = results.Scan(&k.ID, &k.Name, &k.Strength, &k.WeaponPower)
		if err != nil {
			panic(err.Error()) // proper error handling instead of panic in your app
		}
		list = append(list, &k)
	}
	return list
}

func (repository *knightRepository) Save(knight *domain.Knight) {
	_, err := repository.db.Exec(
		"INSERT INTO knight (name, strength, weapon_power) VALUES (?, ?, ?)",
		knight.Name,
		knight.Strength,
		knight.WeaponPower,
	)
	if err != nil {
		panic(err)
	}
}
