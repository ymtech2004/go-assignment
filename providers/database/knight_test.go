package database

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"testing"

	"gitlab.com/zenport.io/go-assignment/domain"

	"github.com/ory/dockertest"
)

var (
	r *knightRepository
)

func TestMain(m *testing.M) {

	// Start a mysql container
	var db *sql.DB
	pool, err := dockertest.NewPool("")
	if err != nil {
		log.Fatalf("Could not connect to docker: %s", err)
	}
	opts := dockertest.RunOptions{
		Repository:   "mysql",
		Tag:          "5.7",
		Env:          []string{"MYSQL_ALLOW_EMPTY_PASSWORD=true", "MYSQL_DATABASE=arena", "MYSQL_USER=zenport", "MYSQL_PASSWORD=zenport"},
		ExposedPorts: []string{"3306"},
	}
	resource, err := pool.RunWithOptions(&opts)
	if err != nil {
		log.Fatalf("Could not start resource: %s", err)
	}

	if err := pool.Retry(func() error {
		var err error
		db, err = sql.Open("mysql", fmt.Sprintf("zenport:zenport@(localhost:%s)/arena", resource.GetPort("3306/tcp")))
		if err != nil {
			return err
		}
		return db.Ping()
	}); err != nil {
		log.Fatalf("Could not connect to docker: %s", err)
	}

	// Create table and insert test data
	_, err = db.Exec("CREATE TABLE knight (id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255) NOT NULL, strength INT NOT NULL, weapon_power INT NOT NULL)  ENGINE=INNODB;")
	if err != nil {
		log.Fatalf("Could not create table: %s", err)
	}
	_, err = db.Exec("INSERT INTO knight (name, strength, weapon_power) VALUES ('player1', 1, 2), ('player2', 2, 3);")
	if err != nil {
		log.Fatalf("Could not insert test data: %s", err)
	}

	r = &knightRepository{db}

	err = pool.Purge(resource)
	if err != nil {
		log.Fatalf("Could not remove container: %s", err)
	}

	os.Exit(0)
}

func TestFind(t *testing.T) {
	k := r.Find("1")
	if k == nil {
		t.Fatalf("Could not find player1 ")
	}
	if k.Name != "player1" {
		t.Fatalf("Get wrong user")
	}
}

func TestSave(t *testing.T) {
	knight := &domain.Knight{Name: "Player3", Strength: 3, WeaponPower: 2}
	r.Save(knight)
}

func TestFindAll(t *testing.T) {
	list := r.FindAll()
	if len(list) != 3 {
		t.Fatalf("Does not get right number of knights ")
	}

}
