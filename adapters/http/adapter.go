package http

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/zenport.io/go-assignment/domain"
	"gitlab.com/zenport.io/go-assignment/engine"
)

type HTTPAdapter struct {
	server *http.Server
}

func (adapter *HTTPAdapter) Start() {
	go func() {
		if err := adapter.server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()
}

func (adapter *HTTPAdapter) Stop() {
	if err := adapter.server.Close(); err != nil {
		log.Fatal("Server Shutdown:", err)
	}
	log.Println("Server exiting")
}

func NewHTTPAdapter(e engine.Engine) *HTTPAdapter {
	r := gin.Default()

	r.POST("/knight", func(c *gin.Context) {
		var knight domain.Knight

		err := c.ShouldBindJSON(&knight)
		if err != nil {
			c.JSON(400, createResponse(400, err.Error()))
		} else {
			e.AddKnight(&knight)
			c.JSON(201, createResponse(201, "Knight is added successfully"))
		}
	})
	r.GET("/knight", func(c *gin.Context) {
		list := e.ListKnights()
		c.JSON(200, list)
	})
	r.GET("/knight/:id", func(c *gin.Context) {
		id := c.Param("id")
		k, err := e.GetKnight(id)
		if err != nil {
			c.JSON(404, createResponse(404, err.Error()))
		} else {
			c.JSON(200, k)
		}
	})

	server := &http.Server{
		Addr:    ":8080",
		Handler: r,
	}

	return &HTTPAdapter{server: server}
}

func createResponse(code int, message string) gin.H {
	return gin.H{
		"code":    code,
		"message": message,
	}
}
