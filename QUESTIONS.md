# Questions

For this assignment you also have to answer a couple of questions.
There is no correct answer and none is mandatory, if you don't know just skip it.

 - **What do you think of the initial project structure ?**
 I think it's good. Logic, adapter, and DB are seperated.


 - **What you will improve from your solution ?**
Make some setting configurable, like DB setting


 - **For you, what are the boundaries of a service inside a micro-service architecture ?**
By its functionality, like access same db table, or same external service.


 - **For you, what are the most relevant usage for SQL, NoSQL, key-value and document store ?**
SQL vs NoSQL
- SQL: predefined structure, complex query and relations between table. ex. transaction data.
- NoSQL: more flexible, does not need predefined structure. ex. logging

key-value vs document store
- key-value: Fast, for only index between key and value, no other complex query. ex. user session
- document store: Data does not have specific structure, or change frequently. ex. logging
