# Start from the latest golang base image
FROM golang:latest

# Download and install the latest release of dep
ADD https://github.com/golang/dep/releases/download/v0.4.1/dep-linux-amd64 /usr/bin/dep
RUN chmod +x /usr/bin/dep

# Set the Current Working Directory inside the container
WORKDIR $GOPATH/src/gitlab.com/zenport.io/go-assignment

# Copy the source from the current directory to the Working Directory inside the container
COPY . ./
RUN dep ensure

# Build the Go app
RUN go build -o main

# Expose port 8080 to the outside world
EXPOSE 8080
ENV DB_HOST DB_PORT

# Command to run the executable
CMD ["./main"]
