package domain

type Fighter interface {
	GetID() string
	GetPower() float64
}

type Knight struct {
	ID          string `json:"id",omitempty`
	Name        string `json:"name" binding:"required"`
	Strength    int    `json:"strength" binding:"required"`
	WeaponPower int    `json:"weapon_power" binding:"required"`
}

func (k *Knight) GetID() string {
	return k.ID
}

func (k *Knight) GetPower() float64 {
	return float64(k.Strength + k.WeaponPower)
}
