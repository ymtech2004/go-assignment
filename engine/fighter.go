package engine

import (
	"errors"
	"fmt"

	"gitlab.com/zenport.io/go-assignment/domain"
)

func (engine *arenaEngine) GetKnight(ID string) (*domain.Knight, error) {
	fighter := engine.knightRepository.Find(ID)
	if fighter == nil {
		return nil, errors.New(fmt.Sprintf("Knight #%s not found.", ID))
	}

	return fighter, nil
}

func (engine *arenaEngine) ListKnights() []*domain.Knight {
	return engine.knightRepository.FindAll()
}

func (engine *arenaEngine) Fight(fighter1ID string, fighter2ID string) domain.Fighter {

	fighter1 := engine.knightRepository.Find(fighter1ID)
	fighter2 := engine.knightRepository.Find(fighter2ID)

	if fighter1 == nil {
		return fighter2
	} else if fighter2 == nil {
		return fighter1
	}

	return engine.arena.Fight(fighter1, fighter2)
}

func (engine *arenaEngine) AddKnight(knight *domain.Knight) {
	engine.knightRepository.Save(knight)
}
